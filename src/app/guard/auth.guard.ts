import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from "@angular/router";
import { RouterExtensions } from "@nativescript/angular";
import { ApplicationSettings } from "@nativescript/core";
import { Observable } from "rxjs";
import { AuthService } from "../core/Services/Auth/Auth.service";

@Injectable({
    providedIn: "root"
})
export class AuthGuard implements CanActivate {

    constructor(
        private authService: AuthService,
        private routerExtensions: RouterExtensions
    ) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (this.authService.hasUserActive()) {
            // Se valida que exista la vaiable en la configuracion de la aplicación
            if (ApplicationSettings.getNumber("userType")) {
                const userType: number = ApplicationSettings.getNumber("userType");
                if (userType === 0) {
                    this.routerExtensions.navigate(["/privacity"], {
                        transition: {
                            name: "fade"
                        },
                        clearHistory: true
                    });
                }
                // Si la variable existe mostrará este mensaje en consola
            } else {
                // Si la variable  no existe se creará y tendrá como valor false
                ApplicationSettings.setBoolean("privacitiy", false);
                this.routerExtensions.navigate(["/privacity"], {
                    transition: {
                        name: "fade"
                    },
                    clearHistory: true
                });
            }

            return true;
        } else {
            this.routerExtensions.navigate(["/login"], {
                transition: {
                    name: "fade"
                },
                clearHistory: true
            });

            return false;
        }
    }
}
