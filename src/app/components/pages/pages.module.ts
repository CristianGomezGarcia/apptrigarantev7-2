import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { PagesRoutingModule } from './pages-router.module';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        PagesRoutingModule
    ],
    exports: [],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PagesModule { }
