import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from '@nativescript/angular';
import { PolicieModel } from '~/app/core/Models/Policie/Policie@Model';

@Component({
    selector: 'app-policies-details',
    templateUrl: 'details.component.html',
    styleUrls: ['details.component.scss']
})

export class DetailsComponent implements OnInit {

    titleActionBar: string = 'Detalles';
    policieDetailsArr: PolicieModel;

    constructor(
        private routerExtension: RouterExtensions,
        private activateRoute: ActivatedRoute
    ) {
        this.policieDetailsArr = JSON.parse(this.activateRoute.snapshot.params.itempolicie);
        const fechaRegistroSplit = this.policieDetailsArr.fechaRegistro.substring(0, 10).split('-');
        this.policieDetailsArr.fechaRegistro = fechaRegistroSplit[2] + ' ' + this.getMonthName(Number(fechaRegistroSplit[1])) + ' ' + fechaRegistroSplit[0];
        this.policieDetailsArr.datos = JSON.parse(this.policieDetailsArr.datos);
        this.titleActionBar = `Póliza No.: ${this.policieDetailsArr.poliza}`;
    }

    private getMonthName(numberMonth: number): string {
        const months = ['Enero', ' Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        return months[numberMonth - 1];
    }

    ngOnInit() { }

    goBack() {
        this.routerExtension.backToPreviousPage();
    }
}